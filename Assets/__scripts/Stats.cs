using UnityEngine;

[CreateAssetMenu]
public class Stats : ScriptableObject
{
    public float maxSpeed;
}
