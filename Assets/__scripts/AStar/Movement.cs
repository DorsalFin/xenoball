using UnityEngine;
using System.Linq;
using Pathfinding;
using UnityEditor;

public class Movement : AIPath
{
    public Alien alien;
    public StateMachine stateMachine;

    [System.Serializable]
    public class StatePathGenerator
    {
        public StateMachine.State state;
        public PathGenerator pathGenerator;
        public bool customSearchLogic;
    }
    public StatePathGenerator[] statePathGenerators;
    StatePathGenerator _currentStatePathGenerator;

    protected override void OnEnable()
    {
        base.OnEnable();
        stateMachine.OnStateSwitch += OnStateSwitched;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        stateMachine.OnStateSwitch -= OnStateSwitched;
    }

    protected override void Update() 
    {
        base.Update();
        if (_currentStatePathGenerator != null) {
            _currentStatePathGenerator.pathGenerator.OnUpdate();
        }
    }

    public override void SearchPath()
    {
        if (float.IsPositiveInfinity(destination.x)) return;
        if (onSearchPath != null) onSearchPath();

        if (_currentStatePathGenerator.customSearchLogic) {
            _currentStatePathGenerator.pathGenerator.SearchPath();
        }
        else
        {
            StartingSearch();
            Vector3 start, end;
            CalculatePathRequestEndpoints(out start, out end);
            seeker.StartPath(start, end);
        }
    }

    public void StartingSearch()
    {
        lastRepath = Time.time;
        waitingForPathCalculation = true;
        seeker.CancelCurrentPathRequest();
    }

    public void OnStateSwitched()
    {
        _currentStatePathGenerator = statePathGenerators.Where(x => x.state == stateMachine.GetState).FirstOrDefault();
        _currentStatePathGenerator.pathGenerator.Initialize(alien);
    }
}

[CustomEditor(typeof(Movement))]
public class MovementEditor : BaseAIEditor
{
    protected override void Inspector () 
    {
        EditorGUILayout.LabelField("ALIEN", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("alien"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("stateMachine"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("statePathGenerators"), true);

        base.Inspector();
    }
}