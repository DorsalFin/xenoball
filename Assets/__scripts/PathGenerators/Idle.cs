using UnityEngine;

[CreateAssetMenu(menuName="PathHandlers/Idle", fileName="Idle")]
public class Idle : PathGenerator
{
    public override void Initialize(Alien alien) {
        base.alien = alien;
        base.alien.movement.destination = base.alien.transform.position;
    }

    public override void SearchPath() {}
    public override void OnUpdate() {}
}
