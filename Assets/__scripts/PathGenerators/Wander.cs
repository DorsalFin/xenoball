using Pathfinding;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "PathHandlers/Wander", fileName = "Wander")]
public class Wander : PathGenerator
{
    public int searchLength;
    [MinMaxSlider(0f, 10f, true)]
    public Vector2 idleTimeRange;
    public float _idleTime;

    public override void Initialize(Alien alien) {
        base.alien = alien;
        base.alien.movement.destination = base.alien.transform.position;
        base.alien.movement.maxSpeed = alien.stats.maxSpeed * speedMod;
    }

    public override void OnUpdate() {}

    public override void SearchPath()
    {
        if (!alien.movement.pathPending && (alien.movement.reachedEndOfPath || !alien.movement.hasPath)) 
        {
            alien.movement.StartingSearch();
            ConstantPath path = ConstantPath.Construct(alien.transform.position, searchLength);
            AstarPath.StartPath(path);
            path.BlockUntilCalculated();
            var singleRandomPoint = PathUtilities.GetPointsOnNodes(path.allNodes, 1)[0];
            alien.movement.seeker.StartPath(alien.transform.position, singleRandomPoint);
            alien.movement.destination = singleRandomPoint;
        }
    }
}
