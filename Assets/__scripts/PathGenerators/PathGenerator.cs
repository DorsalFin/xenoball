using UnityEngine;

public abstract class PathGenerator : ScriptableObject
{
    public float speedMod;
    protected Alien alien;
    public abstract void Initialize(Alien alien);
    public abstract void OnUpdate();
    public abstract void SearchPath();
}
