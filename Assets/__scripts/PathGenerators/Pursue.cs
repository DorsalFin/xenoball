using UnityEngine;

[CreateAssetMenu(menuName="PathHandlers/Pursue", fileName="Pursue")]
public class Pursue : PathGenerator
{
    public override void Initialize(Alien alien) {
        base.alien = alien;
        base.alien.movement.maxSpeed = alien.stats.maxSpeed * speedMod;
    }

    public override void SearchPath() {}

    public override void OnUpdate() {
        alien.movement.destination = alien.targeting.target.transform.position;
    }
}
