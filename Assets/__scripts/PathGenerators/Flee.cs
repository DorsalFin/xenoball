using Pathfinding;
using UnityEngine;

[CreateAssetMenu(menuName="PathHandlers/Flee", fileName="Flee")]
public class Flee : PathGenerator
{
    public int minPathLength;
    public int upToAdditionalPathLength;
    public float aimStrength;

    public override void Initialize(Alien alien) {
        base.alien = alien;
        base.alien.movement.destination = base.alien.transform.position;
        base.alien.movement.maxSpeed = alien.stats.maxSpeed * speedMod;
    }

    public override void OnUpdate() {}

    public override void SearchPath()
    {
        // Call a FleePath call like this, assumes that a Seeker is attached to the GameObject
        Vector3 thePointToFleeFrom = alien.targeting.target.transform.position;
        // Create a path object
        FleePath path = FleePath.Construct (alien.transform.position, thePointToFleeFrom, minPathLength);
        // This is how strongly it will try to flee, if you set it to 0 it will behave like a RandomPath
        path.aimStrength = aimStrength;
        // Determines the variation in path length that is allowed
        path.spread = upToAdditionalPathLength;
        // Start the path and return the result to MyCompleteFunction (which is a function you have to define, the name can of course be changed)
        alien.movement.seeker.StartPath(path, this.PathComplete);
    }

    public void PathComplete(Path p)
    {
        if (!p.error) {
            alien.movement.destination = (Vector3)p.path[p.path.Count - 1].position;
        }
    }
}
