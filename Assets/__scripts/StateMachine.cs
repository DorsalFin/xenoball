// using Pathfinding;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public bool logChanges;

    public enum State {
        Uninitialized, Idle, Flee, Pursue, Wander, Sleep
    }
    public State GetState => _state;
    State _state = State.Uninitialized;
    public delegate void StateSwitch();
    public event StateSwitch OnStateSwitch;

    public void SetState(State newState)
    {
        // ignore if this state is already set
        if (_state == newState) {
            return;
        }

        // log this change if it's turned on
        if (logChanges) {
            Debug.Log(transform.name + " switched state to " + newState);
        }

        // finally cache the new state
        _state = newState;

        // send events to those subscribed
        OnStateSwitch.Invoke();
    }
}
