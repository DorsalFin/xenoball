using UnityEngine;

public class Thing : MonoBehaviour
{
    public Stats stats;
    public Targeting targeting;
    public Mood mood;
}
