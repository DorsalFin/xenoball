using UnityEngine;

public class Mood : MonoBehaviour
{
    public enum MoodTypes {
        Happy,
        Sad,
        Afraid,
        Angry,
        Surprised,
        Disgusted
    }
    public MoodTypes mood;

}
