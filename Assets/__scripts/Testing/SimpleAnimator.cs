using UnityEngine;

public class SimpleAnimator : MonoBehaviour
{
    public Animator animator;
    Alien _alien;

    private void Awake() {
        _alien = GetComponent<Alien>();
    }

	private void Update() {
        float velocity = Mathf.InverseLerp(0f, _alien.stats.maxSpeed, _alien.movement.velocity.magnitude);
        animator.SetFloat("velocity", velocity);
    }
}
