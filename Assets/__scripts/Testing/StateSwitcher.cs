using UnityEngine;

public class StateSwitcher : MonoBehaviour
{
    // the state machines to switch with this script
	public StateMachine[] stateMachines;

    [System.Serializable]
    public struct StateSwitch
    {
        public KeyCode keyCode;
        public StateMachine.State state;
    }
    public StateSwitch[] switches;

    private void Update() 
    {
        for (int i = 0; i < switches.Length; i++)
        {
            if (Input.GetKeyDown(switches[i].keyCode)) {
                SwitchStateMachines(switches[i].state);
                break;
            }
        }
    }

    void SwitchStateMachines(StateMachine.State state)
    {
        for (int i = 0; i < stateMachines.Length; i++) {
            stateMachines[i].SetState(state);
        }
    }
}
