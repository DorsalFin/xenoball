﻿using UnityEngine;

public class MouseTarget : Thing
{
    public new Camera camera;
    public LayerMask groundMask;

    void Update()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, groundMask)) {
            transform.position = hit.point;
        }
    }
}
