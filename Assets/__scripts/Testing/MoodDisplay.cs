using UnityEngine;
using System.Linq;

public class MoodDisplay : MonoBehaviour
{
    public StateMachine stateMachine;

    [System.Serializable]
    public struct StateSprite
    {
        public StateMachine.State state;
        public Sprite sprite;
    }
    public StateSprite[] stateSprites;

    StateMachine.State _displayedState = StateMachine.State.Idle;
    SpriteRenderer _spriteRenderer;

    private void Awake() {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

	private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            _spriteRenderer.enabled = !_spriteRenderer.enabled;
        }

        if (_displayedState != stateMachine.GetState) {
            _spriteRenderer.sprite = stateSprites.Where(x => x.state == stateMachine.GetState).FirstOrDefault().sprite;
            _displayedState = stateMachine.GetState;
        }
    }
}
